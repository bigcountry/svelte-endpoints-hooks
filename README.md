# svelte-endpoints-hooks

A skeleton implementation of a svelte app using hooks to change the response received from a page endpoint.

Setup

```
pnpm i
pnpm run dev
```

navigate to http://localhost:3000/v1/public

normal response

```
{
    "status": "ok",
    "message": "Welcome to Svelte"
}
```

navigate to http://localhost:3000/v1/prive

hooked response

```
{
    "status": "ok",
    "message": "Hooked on Svelte"
}
```