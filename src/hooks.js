/** @type {import('@sveltejs/kit').Handle} */
export async function handle({ event, resolve }) {
    // if this is a private endpoint then change the message
    if (['/v1/private'].some(word => event.url.pathname.startsWith(word))) {
        return new Response(JSON.stringify({ "status": "ok", "message": "Hooked on Svelte" }), { status: 200, headers: { 'Content-Type': 'application/json' } });
    }

    return await resolve(event);
}