/** @type {import('@sveltejs/kit').RequestHandler} */

export async function get() {
    // return a response with a status ok and a message of welcome to svelte
    return {
        body: {
            status: "ok",
            message: "Welcome to Svelte"
        }
    }
}
